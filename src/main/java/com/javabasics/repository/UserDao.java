package com.javabasics.repository;

import com.javabasics.repository.entity.UserEntity;

public interface UserDao {
    Long save (UserEntity userEntity);
    UserEntity findById(Long id);
    UserEntity findByNameAndPassword(String name,String password);


}
