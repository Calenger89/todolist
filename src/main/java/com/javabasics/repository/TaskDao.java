package com.javabasics.repository;

import com.javabasics.repository.entity.TaskEntity;

public interface TaskDao {
    Long save(TaskEntity taskEntity);
    TaskEntity findById(Long id);
    TaskEntity findByUserId(Long userId);
}
