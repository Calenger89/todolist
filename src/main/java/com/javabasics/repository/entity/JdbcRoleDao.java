package com.javabasics.repository.entity;

import com.javabasics.repository.RoleDao;
import connection.ConnectionFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class JdbcRoleDao implements RoleDao {
    private Connection connection = ConnectionFactory.getConnection();
    private ResultSet resultSet = null;

    @Override
    public Collection<RoleEntity> findAllRoles() {
        List<RoleEntity> roleEntities = new ArrayList();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement("SELECT * FROM role");
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                RoleEntity roleEntity = new RoleEntity();
                roleEntity.id = resultSet.getLong("id");
                roleEntity.name = resultSet.getString("name");
                roleEntities.add(roleEntity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return roleEntities;
    }

    @Override
    public RoleEntity findById(Long id) {
        RoleEntity roleEntity = new RoleEntity();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement("SELECT * FROM  role WHERE id =?");
            preparedStatement.setLong(1,id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                roleEntity.id = resultSet.getLong("id");
                roleEntity.name = resultSet.getString("name");
            }
            else  return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return roleEntity;
    }

    @Override
    public Long save(RoleEntity roleEntity) {
        PreparedStatement preparedStatement = null;
        Statement statement = null;
        try {
            statement = connection.createStatement();
            preparedStatement = connection.prepareStatement("INSERT into role (name) VALUES (?)");
            preparedStatement.setString(1,roleEntity.name);
            preparedStatement.executeUpdate();
            resultSet = statement.executeQuery("SELECT LAST_INSERT_ID() as id");
            resultSet.next();
            return resultSet.getLong("id");
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                statement.close();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
