package com.javabasics.repository.entity;

import com.javabasics.repository.UserDao;
import connection.ConnectionFactory;

import java.sql.*;

public class JdbcUserDao implements UserDao {
    private Connection connection = ConnectionFactory.getConnection();


    @Override
    public Long save(UserEntity user) {
        PreparedStatement ps = null;
        Statement statement = null;
        try {
            ps = connection.prepareStatement("insert into user(name,password) values(?,?)");
            ps.setString(1, user.name);
            ps.setString(2, user.password);
            ps.execute();
            statement = connection.createStatement();
            ResultSet rs =statement.executeQuery ("select LAST_INSERT_ID() id");
            rs.next();
            return rs.getLong("id");



        }  catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public UserEntity findById(Long id) {
        UserEntity userEntity = new UserEntity();
        try {
           Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * FROM user where id = " + id);
            while (resultSet.next()) {
                userEntity.id = resultSet.getLong("id");
                userEntity.name = resultSet.getString("name");
                userEntity.password = resultSet.getString("password");

            }
    } catch (SQLException e) {
            e.printStackTrace();
        }
    return userEntity;
}

    @Override
    public UserEntity findByNameAndPassword(String name, String password) {
        UserEntity userEntity = new UserEntity();
        try {
            PreparedStatement statement = connection.prepareStatement("select * FROM user WHERE name =? and password =?");
            statement.setString(1, name);
            statement.setString(2,password);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                userEntity.password = resultSet.getString("password");
                userEntity.name = resultSet.getString("name");
                userEntity.id = resultSet.getLong("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userEntity;
    }


}


