package com.javabasics.repository.entity;

import com.javabasics.repository.TaskDao;
import connection.ConnectionFactory;

import java.sql.*;

public class JdbcTaskDao implements TaskDao {
    private Connection connection = ConnectionFactory.getConnection();

    @Override
    public Long save(TaskEntity task) {
        PreparedStatement ps = null;
        Statement statement = null;
        try {
            ps = connection.prepareStatement("insert into task(name ,user_id) values (?,?)");
            ps.setString(1, task.name);
            ps.setLong(2, task.userId);
            ps.executeUpdate();
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select LAST_INSERT_ID() id");
            rs.next();
            return rs.getLong("id");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


        @Override
        public TaskEntity findById(Long id){
            TaskEntity taskEntity = new TaskEntity();
            try {
                PreparedStatement statement = connection.prepareStatement("select * FROM task where id = ?");
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    taskEntity.id = rs.getLong("id");
                    taskEntity.userId = rs.getLong("user_id");
                    taskEntity.name = rs.getString("name");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return taskEntity;
        }

    @Override
    public TaskEntity findByUserId(Long userId) {
        TaskEntity taskEntity = new TaskEntity();
        try {
            PreparedStatement statement = connection.prepareStatement("select * from task where user_id =? ");
            ResultSet rs = statement.executeQuery();
            while (rs.next()){
                taskEntity.userId = rs.getLong("user_id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return taskEntity;
    }
}



