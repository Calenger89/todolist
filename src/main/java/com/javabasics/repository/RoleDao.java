package com.javabasics.repository;

import com.javabasics.repository.entity.RoleEntity;

import java.util.Collection;

public interface RoleDao {
    Collection<RoleEntity> findAllRoles();
    RoleEntity findById(Long id);
    Long save(RoleEntity roleEntity);
}

