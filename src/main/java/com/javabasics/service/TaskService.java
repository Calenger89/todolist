package com.javabasics.service;

import com.javabasics.service.model.Task;

public interface TaskService {
   Long save (Task task);

   Task findById(Long id);

   Task findByUserId(Long userId);
}
