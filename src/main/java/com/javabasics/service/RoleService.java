package com.javabasics.service;

import com.javabasics.service.model.Role;

import java.util.Collection;

public interface RoleService {
    Long save (Role role);
    Role findById(Long id);
    Collection<Role> findAllRoles();
}
