package com.javabasics.service;

import com.javabasics.repository.RoleDao;
import com.javabasics.repository.entity.RoleEntity;
import com.javabasics.service.model.Role;

import java.util.Collection;
import java.util.stream.Collectors;

public class RoleServiceImpl implements RoleService {
    private RoleDao roleDao;
    public RoleServiceImpl(RoleDao roleDao){
        this.roleDao = roleDao;
    }
    @Override
    public Long save(Role role) {
        return roleDao.save(convertToRoleEntity(role));
    }

    @Override
    public Role findById(Long id) {
        return convertToRole(roleDao.findById(id));
    }



    @Override
    public Collection<Role> findAllRoles() {
        return roleDao.findAllRoles()
                .parallelStream()
                .map(roleEntity -> new Role(roleEntity.id, roleEntity.name))
                .collect(Collectors.toList());
    }
    private RoleEntity convertToRoleEntity(Role role){
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.id = role.id;
        roleEntity.name = role.name;
        return roleEntity;
    }
    private Role convertToRole(RoleEntity roleEntity){
        Role role = new Role();
        role.name = roleEntity.name;
        role.id = roleEntity.id;
        return role;
    }
}
