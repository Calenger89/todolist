package com.javabasics.service.user.model;

public interface UserService {
    Long save(User user);

    User findById(Long id);

    User findByNameAndPassword(String name, String password);

}
