package com.javabasics.service.user.model;

import com.javabasics.repository.UserDao;
import com.javabasics.repository.entity.UserEntity;

public class UserServiceImpl implements UserService {
    public UserDao userDao;

    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    public UserServiceImpl() {

    }


    @Override
    public Long save(User user) {
        return userDao.save(convertToUserEntity(user));
    }

    @Override
    public User findById(Long id) {
        return convertToUser(userDao.findById(id));
    }

    @Override
    public User findByNameAndPassword(String name, String password) {
        return convertToUser(userDao.findByNameAndPassword(name, password));
    }

    private UserEntity convertToUserEntity(User user) {
        UserEntity userEntity = new UserEntity();
        userEntity.name = user.name;
        userEntity.password = user.password;
        return userEntity;
    }

    private User convertToUser(UserEntity userEntity) {
        User user = new User();
        user.id = userEntity.id;
        user.name = userEntity.name;
        user.password = userEntity.password;
        return user;
    }
}
