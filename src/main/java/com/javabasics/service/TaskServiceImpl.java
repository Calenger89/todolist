package com.javabasics.service;

import com.javabasics.repository.TaskDao;
import com.javabasics.repository.entity.TaskEntity;
import com.javabasics.service.model.Task;

public class TaskServiceImpl implements TaskService {
    protected TaskDao taskDao;



    public TaskServiceImpl(TaskDao taskDao) {
        this.taskDao = taskDao;

    }



    public Long save(Task task){
        return taskDao.save(convertToTaskEntity(task));
    }

    private TaskEntity convertToTaskEntity(Task task) {
        TaskEntity taskEntity = new TaskEntity();
        taskEntity.name = task.name;
        taskEntity.userId = task.userId;
        taskEntity.id = task.id;
        return taskEntity;
    }

    @Override
    public Task findById(Long id) {
        return convertToTask(taskDao.findById(id));

    }

    private Task convertToTask(TaskEntity taskEntity) {
        Task task = new Task();
        task.name = taskEntity.name;
        task.userId = taskEntity.userId;
        task.id = taskEntity.id;
        return task;
    }

    @Override
    public Task findByUserId(Long userId) {
        return convertToTask(taskDao.findByUserId(userId));
    }

}
