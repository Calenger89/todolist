package com.javabasics.service.task.user;

import com.javabasics.repository.UserDao;
import com.javabasics.repository.entity.JdbcUserDao;
import com.javabasics.service.user.model.User;
import com.javabasics.service.user.model.UserService;
import com.javabasics.service.user.model.UserServiceImpl;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class UserServiceImplTest {
    private UserDao userDao = new JdbcUserDao();
    private UserService userService = new UserServiceImpl(userDao);


    @Test
    public void idIsNotEmptyAfterSaving() {
        User user = new User();
        user.name = "petr"+ System.currentTimeMillis();
        user.password = "pass";
        Long id = userService.save(user);
        assertTrue(id != null);
    }
    @Test
    public  void  returnedUserId(){
        User user = new User();
        user.name = "ivan"+ System.currentTimeMillis();
        user.password = "pass";
        Long id = userService.save(user);
        User testUser = userService.findById(id);
        Assert.assertEquals(testUser,user);
    }
     @Test
    public void findByNameAndPassword(){
        User user = createUser();
        userService.save(user);
        User returnedUser = userService.findByNameAndPassword(user.name,user.password);
        Assert.assertEquals(user, returnedUser);

     }
     public User createUser(){
        User user = new User();
        user.name = "Ivan" + System.currentTimeMillis();
        user.password = "pass";
        return user;
     }
}