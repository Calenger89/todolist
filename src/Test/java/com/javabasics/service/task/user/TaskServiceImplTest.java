package com.javabasics.service.task.user;

import com.javabasics.repository.TaskDao;
import com.javabasics.repository.entity.JdbcTaskDao;
import com.javabasics.repository.entity.JdbcUserDao;
import com.javabasics.service.TaskService;
import com.javabasics.service.TaskServiceImpl;
import com.javabasics.service.model.Task;
import com.javabasics.service.user.model.User;
import com.javabasics.service.user.model.UserService;
import com.javabasics.service.user.model.UserServiceImpl;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TaskServiceImplTest {
    private TaskService taskService;
    private UserService userService;

    public TaskServiceImplTest() {
        taskService = new TaskServiceImpl(new JdbcTaskDao());
        userService = new UserServiceImpl(new JdbcUserDao());
    }

    @Test
    public void idIsNotEmptyAfterSaving() {
        User user = new User();
        user.name =  "ivan"+ System.currentTimeMillis();
        Long userId = userService.save(user);
        Task task = new Task();
        task.name = "petr"+ System.currentTimeMillis();
        task.userId = userId;
        Long id = taskService.save(task);
        assertTrue(id != null && id!=0 );
    }
    @Test
    public  void  returnedTaskId(){
        User user = new User();
        userService.save(user);
        Task task = new Task();
        task.name = "ivan"+ System.currentTimeMillis();

        Long id = taskService.save(task);
        Task testTask = taskService.findById(id);
        Assert.assertEquals(testTask,task);
    }
    @Test
    public void findByNameAndUserId(){
        Task task = createTask();
        taskService.save(task);
        Task returnedUser = taskService.findByUserId(task.userId);
        Assert.assertEquals(task, returnedUser);

    }
    public Task createTask(){
        Task task = new Task();
        task.name = "Ivan" + System.currentTimeMillis();

        return task;
    }
}
