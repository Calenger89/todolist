package com.javabasics.service.task.user;

import com.javabasics.repository.RoleDao;
import com.javabasics.repository.entity.JdbcRoleDao;
import com.javabasics.service.RoleService;
import com.javabasics.service.RoleServiceImpl;
import com.javabasics.service.model.Role;
import org.junit.Test;

import java.util.Collection;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class RoleServiceImplTest {
    RoleDao roleDao = new JdbcRoleDao();
    RoleService roleServiceImpl = new RoleServiceImpl(roleDao);

    @Test
    public void checkReturnedUserIdAfterSaving(){
        Role role = createRole();
        Long insertedId = roleServiceImpl.save(role);
        System.out.println(insertedId);
        assertTrue(isExist(insertedId));
    }
    @Test
    public void roleByIdIsEqualsSavingRole(){
        Role role = createRole();
        Long insertedId = roleServiceImpl.save(role);
        assertEquals(role,roleServiceImpl.findById(insertedId));
    }
    @Test
    public void checkingAllRolesMapping(){
        Collection<Role> roles = roleServiceImpl.findAllRoles();
        for (Role r:roles){
            System.out.println(r);
        }
    }
    private boolean isExist(Long insertedId){
        return insertedId != null;
    }
    private Role createRole(){
        Role role = new Role();
        role.name = "Admin" + Math.random()*1000;
        return  role;
    }
}
